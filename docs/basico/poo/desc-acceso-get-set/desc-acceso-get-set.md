[TOC]

# Acceso a propiedades con descriptores de acceso get / set

Los **descriptores de acceso [get / set](https://msdn.microsoft.com/es-es/library/w86s7x04.aspx)** son muy útiles para controlar el acceso a los atributos de una clase. Es muy similar a crear un método o función pública para acceder a una propiedad privada y poder controlar los valores de entrada por ejemplo.

- **get** se ejecuta cuando se lee la propiedad. Sino se especifica la propiedad será sólo de escritura.
- **set** se ejecuta cuando se asigna un nuevo valor a la propiedad. Si no lo especificamos la propiedad será sólo de lectura (produce error en tiempo de compilación si tratamos de acceder).

Explicitar los descriptores de acceso tiene muchos usos, el más común es validar los datos antes de modificar un atributo de una clase con `set`.

# Estructura del proyecto

```PS
dotnet new console
```

Aprovechando el ejercicio voy a seguir algunas buenas prácticas, voy a crear un nuevo fichero "PruebaLibroCalificaciones.cs" en una sub-carpeta del proyecto llamada "clases" donde realizaré el ejercicio con descriptores get /set. Para poder acceder desde "Program.cs" debo incluir "PruebaLibroCalificaciones.cs" en el mismo espacio de nombres (`namespace desc_get_set`).

Contenido de [Program.cs]:

```csharp
using System;
using desc_get_set.clases; // <--- Acceso a LibroCalificaciones

namespace desc_get_set {
    class Program {

        static void Main(string[] args) {
            Console.WriteLine("Hello World!");
        }

    }
}
```

# Clase LibroCalificaciones

Nuestra clase de prueba `LibroCalificaciones` (en la sub-carpeta "clases") va a tener una **propiedad privada** de tipo cadena o _string_ con nombre `nombreCurso` (solo accesible desde los métodos de la propia clase pero no desde fuera).

A continuación definimos otra variable publica de tipo string `NombreCurso` (ojo que no es exactamente el mismo nombre) para definir los descriptores de acceso get y set a la propiedad nombreCurso.

```csharp
namespace desc_get_set.clases {

class LibroCalificaciones {
        private string nombreCurso;

        public string NombreCurso {
            get { return nombreCurso; }
            set { nombreCurso = value; }
        }
    }
}
```

**No es una buena práctica** modificar el valor del atributo, por ejemplo:

```CSharp
        public int Number {
            get {
                return number++;   // <--- Mala práctica
            }
        }
```

# Acceso

Probamos los accesos desde la clase principal `Program`:

```csharp
using System;
using desc_get_set.clases; // <--- Acceso a LibroCalificaciones

namespace desc_get_set {
    class Program {

        static void Main(string[] args) {
            LibroCalificaciones librocal = new LibroCalificaciones(); // <---Instancia a nueva clase

            librocal.NombreCurso = "Angular";  // <--- Probamos desc acceso set

            Console.WriteLine("Bienvenido al curso de: "+librocal.NombreCurso); // <--- Probamos desc acceso get
        }
    }
}
```

# Ejecución

Para ejecutar nuestra aplicación de consola:

```PS
dotnet build --configuration Debug
dotnet run
```

Salida de la ejecución, he añadido algunos métodos a la clase `LibroCalificaciones` (ver código fuente):

```PS
set
get
Bienvenido al curso de: Angular
get
Curso: Angular
get
Mi curso es: Angular
```

Para publicar la aplicación como ejecutable para MS Win10:

```ps
dotnet publish -c Release -r win10-x64
```

# Código fuente

- csharp / projects / poo / [desc-get-set](https://gitlab.com/soka/csharp/tree/master/projects/poo/desc-get-set).

# Enlaces internos

- ikerlandajuela.wordpress.com ["C# class LibroCalificaciones: Acceso a propiedades con descriptores de acceso get / set"](https://ikerlandajuela.wordpress.com/2016/10/22/c-class-librocalificaciones-acceso-a-propiedades-con-descriptores-de-acceso-get-set/).

# Enlaces externos

- ["Utilizar propiedades: Guía de programación de C# | Microsoft Docs"](https://docs.microsoft.com/es-es/dotnet/csharp/programming-guide/classes-and-structs/using-properties).
