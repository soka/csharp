[TOC]

Los objetos pueden acceder a una referencia a si mismo usando la palabra clave [`this`](https://msdn.microsoft.com/es-es/library/dk1507sz.aspx). Hace referencia a la instancia actual de la clase. Puede ser usado con el fin de evitar ambigüedades como en este ejemplo:



# Enlaces externos

- ["C# PruebaThis: Referencia a los miembros del objeto actual con “this”"](https://ikerlandajuela.wordpress.com/2016/12/20/c-pruebathis-referencia-a-los-miembros-del-objeto-actual-con-this/).
