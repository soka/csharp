[TOC]

# Proyecto C# de consola con VSCode

Creamos un nuevo proyecto de consola en la ruta que elijamos, vamos a usar VSCode en lugar del IDE típico de Visual Studio.

```PS
dotnet new console
```

La orden crea un esqueleto básico para una aplicación de consola.

Program.cs:

```csharp
using System;

namespace helloworld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```

# Ejecución

Inmediatamente podemos ejecutar la aplicación:

```PS
donet run
```
