[TOC]

![](img/01.PNG)

# Cola FIFO con clases propias

En una cola FIFO (_First In First Out_) el primer elemento en entrar en la cola es el primer elemnto que se extrae.

Voy a implementar dos clases, la clase "Nodo" contiene un atributo y a su vez otra instancia de la clase Nodo dentro de si para apuntar al siguiente nodo.

La clase "Cola" mantiene un atributo de tipo "Nodo" al raíz de la cola y otro al último elemento. Esta clase contendrá los métodos para realizar las operaciones:

- `isVacia()`: Retorna un _booleano_, devuelve `true` si la cola está vacia (el raíz apunta a `null`) o `false` si contiene algún nodo.
- `insertarNodo (int info)`: Recibe como parámetro el atributo del nodo, dentro del método crea una instancia de la clase `Nodo` y lo coloca después del último nodo de la cola.
- `extraerNodo ()`: La operación inversa, retorna un entero con el atributo `info` del nodo, si la cola está vacia (`isVacia`) retorna el valor máximo de un entero con `int.MaxValue` (Int32 de 4 bytes = 2147483647).
- `imprimirCola()`: Recorre la cola y la imprime por consola.

## Clase Nodo

La clase nodo encapsula la información de cada elemento de la cola FIFO. A modo de ejemplo he definido un atributo de tipo entero. La clase `Nodo` define otra clase `Nodo` dentro para apuntar al siguiente elemento de la cola.

```csharp
    class Nodo  {
        public int info;
        public Nodo sig;
    }
```

## Clase Cola

```csharp
    class Cola {
        private Nodo raiz,fondo;

        public Cola() {
            raiz=null;
            fondo=null;
        }
    }
```

### isVacia()

```csharp
        public bool isVacia () {
            if (raiz == null)
                return true;
            else
                return false;
        }
```

### insertarNodo()

```csharp
        public void insertarNodo (int info) {
            Nodo nuevo;
            nuevo = new Nodo ();
            nuevo.info = info;
            nuevo.sig = null;
            if (isVacia ())  {
                raiz = nuevo;
                fondo = nuevo;
            } else {
                fondo.sig = nuevo;
                fondo = nuevo;
            }
        }
```

### extraerNodo()

```csharp
        public int extraerNodo () {
            if (!isVacia ()) {
                int informacion = raiz.info;
                if (raiz == fondo) {
                    raiz = null;
                    fondo = null;
                } else {
                    raiz = raiz.sig;
                }
                return informacion;
            }
            else
                return int.MaxValue;
        }
```

### imprimirCola()

## Ejecución

## Código fuente del ejemplo

## Enlaces

Algunos sistemas _brokers_ de mensajería:

- ["Redis: Almacenamiento de datos NoSQL clave-valor en memoria"](https://ikerlandajuela.wordpress.com/2017/07/23/redis-almacenamiento-de-datos-clave-valor-en-memoria/).
- ["C# BasicMSMQClient: Ejemplo básico de envío de mensaje a una cola MSMQ"](https://ikerlandajuela.wordpress.com/2017/06/19/c-basicmsmqclient-ejemplo-basico-de-envio-de-mensaje-a-una-cola-msmq/).
- ["C# RabbitMQClient: Productor de mensajes básico (con WSO2) usando .NET/C# en VS2012"](https://ikerlandajuela.wordpress.com/2017/04/01/c-rabbitmqclient-productor-de-mensajes-basico-usando-netc-en-vs2012/).
- ["RabbitMQ primeros pasos con colas de mensajería"](https://ikerlandajuela.wordpress.com/2016/05/15/rabbitmq-primeros-pasos-con-colas-de-mensajeria/).

```

```
