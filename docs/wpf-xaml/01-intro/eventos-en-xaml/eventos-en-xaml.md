[TOC]

# Eventos en XAML

El UI de WPF funciona en base a eventos como otros lenguajes de programación, todos los controles incluida la ventana tienen una serie de eventos disponibles, nuestra aplicación se suscribe a estos eventos para que sea notificada cuando se producen y así desarrollar la lógica necesaria en respuesta.

![](img/01.PNG)

# Suscribirse a un evento

En el siguiente ejemplo suscribimos el evento `MouseUp` a un método `pnlMainGrid_MouseUp`

```XAML
<Grid Name="pnlMainGrid" MouseUp="pnlMainGrid_MouseUp" Background="LightBlue">
```

Debemos definir la firma del método en MainWindow.xaml.cs:

```CSharp
private void pnlMainGrid_MouseUp(object sender, MouseButtonEventArgs e)
{
	MessageBox.Show("You clicked me at " + e.GetPosition(this).ToString());
}
```

Nuestra función recibe dos parámetros, el emisor o control que causó el evento y un objeto `MouseButtonEventArgs` con información útil, en este caso obtendremos la posición del cursor llamando al método `GetPosition(IInputElement)`, usa el parámetro de entrada `this` para referirse a la propia clase `MainWindow` respecto a la cual obtiene posición del cursor.

Gracias a [Intellisense](https://es.wikipedia.org/wiki/IntelliSense) el editor de VS autocompleta el código si queremos añadir un nuevo controlador de un evento y crea la función que lo captura.

```XAML
<Grid Name="pnlMainGrid" MouseUp="pnlMainGrid_MouseUp" MouseDown="PnlMainGrid_MouseDown" Background="LightBlue"></Grid>
```

```CSharp
private void PnlMainGrid_MouseDown(object sender, MouseButtonEventArgs e) {

}
```

# Suscribirse a un evento desde el código

La forma de arriba es la más común de suscribirse a un evento pero también podemos hacerlo desde el código. Usando la sintaxis `+=` agregamos un gestor del evento al objeto del evento directamente.

```CSharp
public void EventsSample() {
            InitializeComponent();
            pnlMainGrid.MouseUp += new MouseButtonEventHandler(pnlMainGrid_MouseUp);
        }
```

# Enlaces externos

- wpf-tutorial.com ["Eventos en XAML"](https://wpf-tutorial.com/es/7/xaml/eventos-en-xaml/).
- ["UIElement.MouseUp Event (System.Windows) | Microsoft Docs"](https://docs.microsoft.com/es-es/dotnet/api/system.windows.uielement.mouseup?view=netframework-4.8).
- ["MouseButtonEventArgs Class (System.Windows.Input) | Microsoft Docs"](https://docs.microsoft.com/en-us/dotnet/api/system.windows.input.mousebuttoneventargs?view=netframework-4.8).

```

```
