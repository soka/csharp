[TOC]

# Introducción

Voy a desarrollar una aplicación básica con C# y Windows Presentation Foundation (WPF). Este tutorial está realizado con Visual Studio Community 2017.

WPF usa Extensible Application Markup Language (XAML) como "lenguaje" de definición de las ventanas y controles.

# Crear proyecto

Creamos un nuevo proyecto de tipo "Aplicación de WPF" en VS y lo nombramos como "HelloWorld". El proyecto consta de dos ficheros principales, "MainWindow.xaml" que define mediante una sintaxis de etiquetas como XML la parte visual y "MainWindow.xaml.cs" que contien la lógica de la aplicación en C#.

![](img/01.PNG)

"MainWindow.xaml" está divido en dos pantallas, una es la ventana de diseño donde vamos viendo el aspecto que tiene nuestra ventana y la inferior es el código fuente. En una aplicación WPF podemos diseñar el UI de dos formas, arrastrando y soltando los componentes o editando el propio XAML.

![](img/02.PNG)

Este el contenido de "MainWindow.xaml":

```XAML
<Window x:Class="HelloWorld.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:HelloWorld"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <Grid>

    </Grid>
</Window>
```

Por defecto se define un elemento "Grid" (rejilla) para la ventana.

# Añadiendo nuestro primer control, un TextBlock

Arrastramos y soltamos un control "TextBlock" a la ventana, automáticamente se genera el código XAML correspondiente:

```XAML
<TextBlock HorizontalAlignment="Left" TextWrapping="Wrap" Text="TextBlock" VerticalAlignment="Top" RenderTransformOrigin="4.571,8.188"/>
```

Si cambiamos el atributo `Text` del elemento veremos como cambiar en la vetana de diseño también.

![](img/03.PNG)

Ahora compilamos y ejecutamos la aplicación:

![](img/04.PNG)

¡Ya tenemos nuestra primera aplicación!

# Enlaces internos

- ["WPF - Hello World"](https://www.tutorialspoint.com/wpf/wpf_hello_world.htm).
- ["MVVMBasicDemo1: Patrón de diseño MVVM con aplicaciones basadas en WPF Resultado de imagen de WPF (Windows Presentation Foundation)"](https://ikerlandajuela.wordpress.com/2017/07/26/mvvmbasicdemo1-patron-de-diseno-mvvm-con-aplicaciones-basadas-en-wpf-resultado-de-imagen-de-wpf-windows-presentation-foundation/). [Proyecto en GitHub](https://github.com/ikerlandajuela/MVVMBasicDemo1).
- [Enlace](https://ikerlandajuela.wordpress.com/category/programacion/csharp/) artículos programación C# en mi blog WP

# Enlaces externos

- ["Tutorial: Mi primera aplicación de escritorio WPF"](https://docs.microsoft.com/es-es/dotnet/framework/wpf/getting-started/walkthrough-my-first-wpf-desktop-application) Tutorial original oficial de MS origen de este artículo.

```

```
