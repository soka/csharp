[TOC]

# Introducción

Cuando creamos un nuevo proyecto de tipo aplicación WPF se crea el siguiente código:

```XAML
<Window x:Class="HelloWorld.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:HelloWorld"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <Grid>

    </Grid>
</Window>
```

El elemento `Window` es el nodo raíz y define la ventana de la aplicación, `x:Class="HelloWorld.MainWindow"` es una declaración parcial de una clase y conecta el XAML con la clase `MainWindow` declarada en MainWindow.xaml.cs. Luego define los espacios de nombres del lenguaje XAML.

Después viene la definición de un objeto tipo rejilla o grid.

# Ejemplo XAML, un botón

Ahora modifico el XAML e introduzco a mano un botón con un texto, altura y anchura del control:

```XAML
<Button Content = "Click Me" Height = "30" Width = "60" />
```

Una sintaxis alternativa que hace lo mismo:

```XAML
<Button>
   <Button.Content>Click Me</Button.Content>
   <Button.Height>30</Button.Height>
   <Button.Width>60</Button.Width>
</Button>
```

# El mismo botón en CSharp

Podemos hacer lo mismo desde el código C# en MainWindow.xaml.cs en el método constructor `MainWindow` de la clase del mismo nombre:

```CSharp
            // Create the StackPanel
            StackPanel stackPanel = new StackPanel();
            this.Content = stackPanel;

            // Create the Button
            Button button = new Button();
            button.Content = "Haz click";
            button.HorizontalAlignment = HorizontalAlignment.Left;
            button.Margin = new Thickness(150);
            button.VerticalAlignment = VerticalAlignment.Top;
            button.Width = 75;
            stackPanel.Children.Add(button);
```

# Enlaces externos

- .tutorialspoint.com ["WPF - XAML Overview"](https://www.tutorialspoint.com/wpf/wpf_xaml_overview.htm).

```

```
