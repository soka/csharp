[TOC]

# Programación C# con Visual Studio Code

Requisitos dotnet:

- [.NET Core](https://dotnet.microsoft.com/download). .NET Framework 4.7.2 Developer Pack _"The developer pack is used by software developers to create applications that run on .NET Framework, typically using Visual Studio."_
- [C# for Visual Studio Code (powered by OmniSharp)](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp).

Creando mi primer proyecto de consola:

```
dotnet new console
```
