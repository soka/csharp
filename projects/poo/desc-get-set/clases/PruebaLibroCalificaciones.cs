using System;

namespace desc_get_set.clases {

    class LibroCalificaciones {
        private string nombreCurso ="";

        public string NombreCurso {
            get { 
                Console.WriteLine("get");
                return nombreCurso; 
            }
            set { 
                Console.WriteLine("set: "+value);
                if (value.Length>0) nombreCurso = value; 
                    else Console.WriteLine("error cadena vacía!");                       
            }
        }        

        private int number;
        public int Number {
            get {
                return number++;   // <--- Mala práctica
            }
        }

        public string dameCurso() {
            return this.NombreCurso;
        }        

        public void muestraCurso() {
            Console.WriteLine("Curso: "+this.NombreCurso);
        }
    } 

    
}